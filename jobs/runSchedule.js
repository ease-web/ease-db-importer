import moment from 'moment';
import dataMigrateJob from './dataMigrate';
import systemConfig from '../config/config.json';

const schedule = require('node-schedule');


const startSchedule = () => {
  const { startTime, fromDate, toDate } = systemConfig.batchSetting;
  const startHour = startTime.substring(0, 2);
  const startMin = startTime.substring(3);
  const currentDate = moment(new Date()).format('YYYY-MM-DD');

  const rule = new schedule.RecurrenceRule();
  rule.tz = 'Asia/Singapore';

  rule.second = 0;
  rule.minute = startMin;
  rule.hour = startHour;

  if (currentDate >= fromDate && currentDate <= toDate) {
    // schedule.scheduleJob(`00 ${startMin} ${startHour} * * *`, () => {
    schedule.scheduleJob(rule, () => {
      dataMigrateJob.dataMigrate();
    });
  }
};

module.exports = {
  startSchedule,
};
