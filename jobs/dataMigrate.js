/* eslint-disable no-param-reassign */
import * as _ from 'lodash';
// import { getAllDocs, getDoc } from '../util/couchbase';
import {
  // eslint-disable-next-line no-unused-vars
  getAllDocsByType, getAllDocsContentByType, getDoc,
  // eslint-disable-next-line no-unused-vars
  getAllDocsContentByTypesAndIDs, getAttachment, updToSG,
  getAllDeltaDocs, getAllDocsContentByID,
  getTypesAndIDsByRange, dataTypeStatistics, getCollectionNameById,
} from '../util/couchbase';
import printLogWithTime from '../util/log';

import {
  insertRecord, getDocs, removeDocs, findOneAndUpdate, getModelByType,
  insertMany, getNextID, addAttachment, replaceDot, searchCnt,
} from '../util/documentDB';

import dataMigrationLogModel from '../models/dataMigrationLog';
import testDataModel from '../models/testData';
import migrateStatusModel from '../models/migrateStatus';
import dataMigrationUnexpectCaseModel from '../models/dataMigrationUnexpectCase';
import dataExtractionErrorLogModel from '../models/dataExtractionErrorLog';
// import couchBaseBackupModel from '../models/couchBaseBackup';
import systemConfig from '../config/config.json';
// import { masterDataImport } from './dataImport';

const moment = require('moment');


let totalRecordsQuantity;
let ranRecordsQuantity;
let successCases = [];
let failedCases = [];
let s3Logs = [];
let s3LogsDelta = [];
let runDate;
let endTime;
const migrateStatus = {};
const { duration, startTime } = systemConfig.batchSetting;

const dataFormatTransfer = (previousData, previousID) => {
  const clonedPreviousData = _.cloneDeep(previousData);
  // {_id: {}, xxx} ----> {id: {}, xxx}
  clonedPreviousData.id = previousID;
  delete clonedPreviousData[`${'_id'}`];
  // {_attachments: {}, xxx} ----> {attachments: {}, xxx}
  clonedPreviousData.attachments = clonedPreviousData[`${'_attachments'}`];
  delete clonedPreviousData[`${'_attachments'}`];
  if (!clonedPreviousData.rev) {
    clonedPreviousData.rev = clonedPreviousData[`${'_rev'}`];
  }

  _.forEach(clonedPreviousData.attachments, (attachement, key) => {
    _.forOwn(attachement, () => {
      delete attachement[`${'digest'}`];
      delete attachement[`${'revpos'}`];
      delete attachement[`${'stub'}`];
      attachement.key = `${clonedPreviousData.id}-${key}`;
    });
  });

  // Remove _rev
  // delete clonedPreviousData[`${'_rev'}`];
  return replaceDot(clonedPreviousData);
  // return clonedPreviousData;
};
/*
const migrateFinishQuantity = 0;
const migrateSkipQuantity = 0;
*/


const getTypeByCollection = (collection) => {
  const { importToTestTable, isDebugMode } = systemConfig.debugSetting;
  if (importToTestTable && isDebugMode) {
    return testDataModel;
  }
  switch (collection) {
    case 'customer': return 'cust';
    case 'agent': return 'agent';
    case 'agentExtraInfo': return 'userExt';
    case 'fna': return 'bundle';
    case 'fnaFe': return 'fe';
    case 'fnaNa': return 'na';
    case 'fnaPda': return 'pda';
    case 'quotation': return 'quotation';
    case 'application': return 'application';
    case 'approval': return 'approval';
    case 'shieldApplication': return 'masterApplication';
    case 'shieldApproval': return 'masterApproval';
    case 'seqMaster': return 'seq';
    case 'apiResponse': return 'API_RESPONSE';
    default: return 'other';
  }
};

const getCollenctionByType = (type) => {
  switch (type) {
    case 'cust': return 'customer';
    case 'agent': return 'agent';
    case 'userExt': return 'agentExtraInfo';
    case 'bundle': return 'fna';
    case 'fe': return 'fnaFe';
    case 'na': return 'fnaNa';
    case 'pda': return 'fnaPda';
    case 'quotation': return 'quotation';
    case 'application': return 'application';
    case 'approval': return 'approval';
    case 'masterApplication': return 'shieldApplication';
    case 'masterApproval': return 'shieldApproval';
    case 'seq': return 'seqMaster';
    case 'API_RESPONSE': return 'apiResponse';
    default: return 'masterData';
  }
};

const updateErrorLog = async (log) => {
  const lastErrorLog = await getDocs(dataExtractionErrorLogModel, {}, {});
  let jsonList = [];
  let attachmentList = [];

  if (lastErrorLog.length > 0) {
    // eslint-disable-next-line no-underscore-dangle
    jsonList = jsonList.concat(lastErrorLog[0]._doc.jsonList);
    // eslint-disable-next-line no-underscore-dangle
    attachmentList = attachmentList.concat(lastErrorLog[0]._doc.attachmentList);
  }
  if (log.transactions && log.transactions.length > 0) {
    _.forEach(log.transactions, (tran) => {
      if (tran.result === 'failed') {
        jsonList.push({
          id: tran.docID,
          failedReason: tran.failedReason,
          batchID: log.id,
        });
      }
      if (tran.attachments && tran.attachments.length > 0) {
        _.forEach(tran.attachments, (attach) => {
          if (attach.result === 'failed') {
            attachmentList.push({
              id: attach.key,
              failedReason: attach.failedReason,
              batchID: log.id,
            });
          }
        });
      }
    });
  }
  const newErrorLog = {};
  newErrorLog.jsonFailed = jsonList.length;
  newErrorLog.attachmentFailed = attachmentList.length;
  newErrorLog.jsonList = jsonList;
  newErrorLog.attachmentList = attachmentList;


  await removeDocs(dataExtractionErrorLogModel, {});
  await insertRecord(dataExtractionErrorLogModel, newErrorLog);
};

const updateRunningID = async (docs, runingType) => {
  const runingIDs = [];
  if (docs.total_rows > 0) {
    _.forEach(docs.rows, (doc) => {
      runingIDs.push(doc.id);
    });
  }
  migrateStatus.runingIDs = runingIDs;
  migrateStatus.runingType = runingType;
  await findOneAndUpdate(migrateStatusModel, { id: migrateStatus.id }, migrateStatus);
};

const insertLog = async (sucesses, fails, startT, batchNumber,
  s3Result, logType, deletedJson, type) => {
  if (!batchNumber) {
    batchNumber = await getNextID('dataMigrationLog');
  }
  const log = {};
  const transactions = [];
  const endType = type;
  let warningCnt = 0;
  _.forEach(sucesses.concat(fails), (doc) => {
    const transaction = {};
    transaction.docID = doc.id;
    // endType = doc.type ? doc.type : doc.TYPE;
    transaction.collection = getCollenctionByType(doc.type ? doc.type : doc.TYPE);
    // eslint-disable-next-line no-underscore-dangle
    transaction.mongoID = doc._id;
    transaction.result = doc.failedReason ? 'failed' : 'passed';
    transaction.failedReason = doc.failedReason;
    transaction.statement = doc.statement ? doc.statement : 'insert';
    _.forEach(s3Result.transactions, (t) => {
      if (t.docID === doc.id) {
        transaction.attachments = t.attachments;
      }
    });
    if (doc.isWarning) {
      transaction.result = 'warning';
      transaction.warningReason = doc.warningReason;
      warningCnt += 1;
    }
    transactions.push(transaction);
  });

  const batchID = `dataMigrationLog_${batchNumber}`;
  log.id = batchID;
  log.migrationType = logType;
  log.batchNumber = batchNumber;
  log.startTime = startT;
  log.endTime = new Date();
  log.jsonWarning = warningCnt;
  log.jsonFailed = fails.length - warningCnt;
  log.jsonSuccess = sucesses.length;
  if (deletedJson) {
    // log.deleteJsonSuccess = deletedJson.deleted;
    // log.deleteJsonFailed = deletedJson.toDelete - deletedJson.deleted;
  }
  log.attachmentFailed = s3Result.attachmentFailed ? s3Result.attachmentFailed : 0;
  log.attachmentSuccess = s3Result.attachmentSuccess ? s3Result.attachmentSuccess : 0;
  log.attachemntWarning = s3Result.attachemntWarning ? s3Result.attachemntWarning : 0;
  log.transactions = transactions;
  await insertRecord(dataMigrationLogModel, log);
  if (log.attachmentFailed > 0 || log.jsonFailed > 0) {
    await updateErrorLog(log);
  }

  ranRecordsQuantity = ranRecordsQuantity + sucesses.length + fails.length;

  // update migrate log for contentious the batch
  if (transactions.length > 0) {
    // let endType = transactions[transactions.length - 1].collection;
    /*
    if (type === 'other') {
      endType = 'zzother';
    } else if (type === 'unexpect') {
      endType = 'zzzunexpect';
    }
    */
    migrateStatus.lastEndType = endType;
    migrateStatus.lastEndID = transactions[transactions.length - 1].docID;
    migrateStatus.endTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS');
    migrateStatus.TotalRecord = ranRecordsQuantity;
    migrateStatus.runingIDs = [];
    migrateStatus.runingType = '';
    await findOneAndUpdate(migrateStatusModel, { id: migrateStatus.id }, migrateStatus);
  }

  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime(`Batch Number             : ${batchNumber}`);
  printLogWithTime(`Transaction Type         : ${logType}`);
  printLogWithTime(`Total json (Batch)       : ${sucesses.length + fails.length}`);
  printLogWithTime(`Total attachment (Batch) : ${log.attachmentSuccess + log.attachmentFailed + log.attachemntWarning}`);
  printLogWithTime(`Json Migrated            : ${sucesses.length} passed, ${fails.length - warningCnt} failed, ${warningCnt} warning`);
  printLogWithTime(`Attachment Migrated      : ${log.attachmentSuccess} passed, ${log.attachmentFailed} failed, ${log.attachemntWarning} warning`);
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime(' ');
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime(`Overall Status           : ${ranRecordsQuantity}/${totalRecordsQuantity}`);
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime(' ');
};

const migrateOneJson = async (type, id) => {
  try {
    let error = false;

    const docByID = await getAllDocsContentByID(id);
    // await insertMany(couchBaseBackupModel, replaceDot(docs.rows)); for backup couch base
    // create view structure
    const row = { id, key: [id], value: docByID };
    const rows = [];
    rows.push(row);
    const docs = { total_rows: 1, rows };
    if (!systemConfig.debugSetting.isDebugMode || systemConfig.debugSetting.runUploadS3) {
      s3Logs = await addAttachment(docs, getCollenctionByType(type));
      s3LogsDelta.push(s3Logs);
    }

    // await getAttachment('QU001001-00031', 'proposal');
    // const test = docs.total_rows;
    if (docs.total_rows > 0) {
      const doc = dataFormatTransfer(docs.rows[0].value, docs.rows[0].id);
      const model = getModelByType(type);
      const cnt2 = await searchCnt(model, { id: doc.id });
      if (cnt2 > 0) {
        await findOneAndUpdate(model, { id: doc.id }, doc)
          .then(() => {
            const successCase = doc;
            successCase.statement = 'update';
            successCases = successCases.concat(successCase);
          }).catch((err) => {
            error = err;
          });
        if (error) {
          printLogWithTime(`ID: ${doc.id} failed`);
          printLogWithTime(error);
          const failCase = doc;
          failCase.failedReason = error.message;
          failCase.statement = 'update';
          failedCases = failedCases.concat(failCase);
        }
      } else {
        await insertRecord(model, doc)
          .then(() => {
            const successCase = doc;
            successCase.statement = 'insert';
            successCases = successCases.concat(successCase);
          }).catch((err) => {
            error = err;
          });
        if (error) {
          printLogWithTime(`ID: ${doc.id} failed`);
          printLogWithTime(error);
          const failCase = doc;
          failCase.failedReason = error.message;
          failCase.statement = 'insert';
          failedCases = failedCases.concat(failCase);
        }
      }
    }
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
};

const migrateDeltaRecords = async (records) => {
  let p = Promise.resolve();
  const allTypeID = Object.entries(records).sort((a, b) => (
    (a[0].toLowerCase() > b[0].toLowerCase()) ? 1 : -1));
  _.forEach(allTypeID, (IDs) => {
    const typeIDs = IDs[1];
    const type = IDs[0];
    _.forEach(typeIDs, (id) => {
      p = p.then(() => migrateOneJson(type, id));
    });
  });
  return p;
};

const migrateUnexpectRecords = async (records) => {
  let p = Promise.resolve();
  _.forEach(records, (record) => {
    p = p.then(() => migrateOneJson(record.type, record.id));
  });

  return p;
};

const migrateHalfBatch = async (model, batchForUpload, startT, batchNumber, type) => {
  const batch1 = [];
  const batch2 = [];
  _.forEach(batchForUpload, (doc, index) => {
    if (index < batchForUpload.length / 2) {
      batch1.push(doc);
    } else {
      batch2.push(doc);
    }
  });

  let nextHalf1 = false;
  await insertMany(model, batch1)
    .then((res) => {
      if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
        printLogWithTime(`update success: Type = ${batch1[0].type}, ID From ${batch1[0].id} to ${batch1[batch1.length - 1].id}, total ${batch1.length} `);
      }
      successCases = successCases.concat(res);
    }).catch((error) => {
      if (batch1.length === 1) {
        // if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
        // printLogWithTime(`update failed: Type = ${batch1[0].type}, ID From ${batch1[0].id}
        // to ${batch1[batch1.length - 1].id}, total ${batch1.length} `);
        printLogWithTime(`ID: ${batch1[0].id} failed`);
        printLogWithTime(error);
        // }
        const failCase = batch1[0];
        failCase.failedReason = error.message;
        failedCases = failedCases.concat(failCase);
      } else {
        nextHalf1 = true;
      }
    });
  if (nextHalf1) {
    const removeID = [];
    _.forEach(batch1, (doc) => {
      removeID.push(doc.id);
    });
    await removeDocs(model, { id: { $in: removeID } });
    await migrateHalfBatch(model, batch1);
  }

  let nextHalf2 = false;
  await insertMany(model, batch2)
    .then((res) => {
      if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
        printLogWithTime(`update success: Type = ${batch2[0].type}, ID From ${batch2[0].id} to ${batch2[batch2.length - 1].id}, total ${batch2.length} `);
      }
      successCases = successCases.concat(res);
    }).catch((error) => {
      if (batch2.length === 1) {
        // if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
        // printLogWithTime(`update failed: Type = ${batch2[0].type}, ID From ${batch2[0].id}
        // to ${batch2[batch2.length - 1].id}, total ${batch2.length} `);
        printLogWithTime(`ID: ${batch2[0].id} failed`);
        printLogWithTime(error);
        // }
        const failCase = batch2[0];
        failCase.failedReason = error.message;
        failedCases = failedCases.concat(failCase);
      } else {
        nextHalf2 = true;
      }
    });
  if (nextHalf2) {
    const removeID = [];
    _.forEach(batch2, (doc) => {
      removeID.push(doc.id);
    });
    await removeDocs(model, { id: { $in: removeID } });
    await migrateHalfBatch(model, batch2);
  }
  if (batchNumber) {
    await insertLog(successCases, failedCases, startT, batchNumber, s3Logs, 'full', false, type);
  }
};


const migrateBatch = async (type, start, end) => {
  try {
    let error = false;
    const startT = new Date();
    const current = moment.utc(new Date()).add(8, 'hour').format('YYYYMMDDHHmm');
    // printLogWithTime(`current = ${current}`);
    // printLogWithTime(`utc time= ${moment.utc(new Date()).format('YYYYMMDDHHmm')}`);
    // printLogWithTime(`local time= ${moment(new Date()).format('YYYYMMDDHHmm')}`);
    let returnResult = {};
    if (current < endTime) {
      const docs = await getAllDocsContentByTypesAndIDs(type, start, end);
      const excludeList = ['08_product_NPE_2', 'testingNPE'];

      _.forEach(docs.rows, (doc, indx) => {
        _.forEach(excludeList, (item) => {
          if (doc && doc.id === item) {
            docs.rows.splice(indx, 1);
          }
        });
      });
      /*
      let resultMultiple = 50;
      const total = docs.total_rows;
      const { rows } = docs;
      while (resultMultiple > 1) {
        docs.rows = docs.rows.concat(rows);
        docs.total_rows += total;
        resultMultiple -= 1;
      }
      printLogWithTime(`docs.rows.length = ${docs.rows.length}`);
      printLogWithTime(`total_rows = ${docs.total_rows}`);

*/
      await updateRunningID(docs, type);

      // await insertMany(couchBaseBackupModel, replaceDot(docs.rows)); for backup couch base
      if (!systemConfig.debugSetting.isDebugMode || systemConfig.debugSetting.runUploadS3) {
        s3Logs = await addAttachment(docs, getCollenctionByType(type));
      }
      // handle cases that cannot get from view of CB.
      const typeAndIDs = await getTypesAndIDsByRange(type, start, end);

      if (typeAndIDs.total_rows > 0) {
        _.forEach(typeAndIDs.rows, (doc, indx) => {
          _.forEach(excludeList, (item) => {
            if (doc && doc.id === item) {
              typeAndIDs.rows.splice(indx, 1);
            }
          });
        });
        // eslint-disable-next-line no-restricted-syntax
        for (const typeAndID of typeAndIDs.rows) {
          let exist = false;

          _.forEach(docs.rows, (doc) => {
            if (doc.id === typeAndID.id) {
              exist = true;
            }
          });
          if (!exist) {
            if (systemConfig.debugSetting.isDebugMode
                  && systemConfig.debugSetting.showMorelog) {
              printLogWithTime(`${typeAndID.id} cannot get doc from the view of CB.`);
            }
            // eslint-disable-next-line no-await-in-loop
            const unexpectDocs = await getDocs(dataMigrationUnexpectCaseModel,
              { id: 'CannotGetDocFromView' }, { id: 'desc' });
            // eslint-disable-next-line no-await-in-loop
            // await migrateOneJson(type, typeAndID.id);
            if (unexpectDocs.length > 0) {
              // eslint-disable-next-line no-underscore-dangle
              const unexpect = unexpectDocs[0]._doc.unexpectIds;
              let existed = false;
              _.forEach(unexpect, (doc) => {
                if (doc.id === typeAndID.id) {
                  existed = true;
                }
              });
              if (!existed) {
                unexpect.push({ type, id: typeAndID.id });
              }
              // eslint-disable-next-line no-await-in-loop
              await findOneAndUpdate(dataMigrationUnexpectCaseModel, { id: 'CannotGetDocFromView' }, { id: 'CannotGetDocFromView', unexpectIds: unexpect });
            } else {
              // eslint-disable-next-line no-await-in-loop
              await insertRecord(dataMigrationUnexpectCaseModel, { id: 'CannotGetDocFromView', unexpectIds: [{ type, id: typeAndID.id }] });
            }
            /*
            s3Logs.attachmentSuccess = (s3Logs.attachmentSuccess ? s3Logs.attachmentSuccess : 0)
                 + s2LogsMore.attachmentSuccess;
            s3Logs.attachmentFailed = (s3Logs.attachmentFailed ? s3Logs.attachmentFailed : 0)
                + s2LogsMore.attachmentFailed;
            s3Logs.transactions = s3Logs.transactions.push(s2LogsMore.transactions);
            */
          }
        }
      }

      // await getAttachment('QU001001-00031', 'proposal');
      // const test = docs.total_rows;
      if (docs.total_rows > 0) {
        const batchForUpload = [];
        _.forEach(docs.rows, (doc) => {
          // for (const doc of docs.rows) {
        /*
        if (type === 'seqMaster') {
          if (doc.id.substring(doc.id.length - 4) === '-seq' || doc.id === 'agentNumberMap') {
            batchForUpload.push(dataFormatTransfer(doc.value, doc.id));
          }
        } else if (type === 'null') {
          if (!(doc.id.substring(doc.id.length - 4) === '-seq' || doc.id === 'agentNumberMap')) {
            batchForUpload.push(dataFormatTransfer(doc.value, doc.id));
          }
        } else {
          */
          batchForUpload.push(dataFormatTransfer(doc.value, doc.id));
        // }
        });
        const model = getModelByType(type);
        const batchNumber = await getNextID('dataMigrationLog');
        await insertMany(model, batchForUpload)
          .then((result) => {
            if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
              printLogWithTime(`update success: Type = ${type}, ID From ${start} to ${end}, total ${batchForUpload.length} `);
            }
            returnResult = result;
          }).catch(() => {
            error = true;
          });
        if (error) {
          const removeID = [];
          _.forEach(batchForUpload, (doc) => {
            removeID.push(doc.id);
          });
          await removeDocs(model, { id: { $in: removeID } });
          successCases = [];
          failedCases = [];
          await migrateHalfBatch(model, batchForUpload, startT, batchNumber, type);
        } else {
          await insertLog(returnResult, [], startT, batchNumber, s3Logs, 'full', false, type);
        }
      }
    }
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
};


const handleUnexpect = async () => {
  const unexpectDocs = await getDocs(dataMigrationUnexpectCaseModel,
    { id: 'CannotGetDocFromView' }, { id: 'desc' });
  if (unexpectDocs.length > 0) {
    const startT = new Date();
    successCases = [];
    failedCases = [];
    s3LogsDelta = [];

    // eslint-disable-next-line no-underscore-dangle
    await migrateUnexpectRecords(unexpectDocs[0]._doc.unexpectIds);

    const batchLog = {};
    let attachmentSuccess = 0;
    let attachmentFailed = 0;
    let attachemntWarning = 0;
    const transactions = [];
    _.forEach(s3LogsDelta, (log) => {
      attachmentSuccess += log.attachmentSuccess;
      attachmentFailed += log.attachmentFailed;
      attachemntWarning += log.attachemntWarning;
      transactions.push(log.transactions[0]);
    });
    batchLog.attachmentSuccess = attachmentSuccess;
    batchLog.attachmentFailed = attachmentFailed;
    batchLog.attachemntWarning = attachemntWarning;
    batchLog.transactions = transactions;
    // eslint-disable-next-line no-await-in-loop
    await insertLog(successCases, failedCases, startT, null, batchLog, 'full', false, 'zzzunexpect');

    printLogWithTime('');
  }
};

const migrateRecords = async (records, numOfBatch, lastEndType) => {
  let importDocTypes = systemConfig.debugSetting.importSpecifiedDocTypes;
  if (!systemConfig.debugSetting.isDebugMode) {
    importDocTypes = [];
  }
  let needImport = true;
  if (importDocTypes != null && importDocTypes.length > 0) {
    needImport = false;
  }
  // let p = Promise.resolve();
  /*
  const allTypeID = Object.entries(records).sort((a, b) => (
    (a[0].toLowerCase() > b[0].toLowerCase()) ? 1 : -1));
    */

  const allTypeID = Object.entries(records).sort((a, b) => {
    let aValue = a[0].toLowerCase();
    if (aValue === 'null') {
      aValue = 'aaaa';
    }
    if (aValue === 'other') {
      aValue = 'zzzz';
    }

    let bValue = b[0].toLowerCase();
    if (bValue === 'null') {
      bValue = 'aaaa';
    }
    if (bValue === 'other') {
      bValue = 'zzzz';
    }

    if (aValue > bValue) {
      return 1;
    }
    if (aValue < bValue) {
      return -1;
    }
    return 0;
  });
  // console.log(allTypeID);
  // _.forEach(allTypeID, (IDs) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const IDs of allTypeID) {
    const typeIDs = IDs[1];
    const type = IDs[0];
    if (importDocTypes != null && importDocTypes.length > 0) {
      needImport = importDocTypes.includes(type);
    }
    if (needImport) {
      if (type === 'other') {
        if (lastEndType !== 'zzother' && lastEndType !== 'zzzunexpect') {
          const startT = new Date();
          const current = moment.utc(new Date()).add(8, 'hour').format('YYYYMMDDHHmm');
          if (current < endTime) {
            successCases = [];
            failedCases = [];
            s3LogsDelta = [];

            const otherRecords = [];
            _.forEach(typeIDs, (id, index) => {
              otherRecords.push({ type, id: typeIDs[`${index}`] });
            });
            if (otherRecords.length > 0) {
            // eslint-disable-next-line no-await-in-loop
              await migrateUnexpectRecords(otherRecords);

              const batchLog = {};
              let attachmentSuccess = 0;
              let attachmentFailed = 0;
              let attachemntWarning = 0;
              const transactions = [];
              _.forEach(s3LogsDelta, (log) => {
                attachmentSuccess += log.attachmentSuccess;
                attachmentFailed += log.attachmentFailed;
                attachemntWarning += log.attachemntWarning;
                transactions.push(log.transactions[0]);
              });
              batchLog.attachmentSuccess = attachmentSuccess;
              batchLog.attachmentFailed = attachmentFailed;
              batchLog.attachemntWarning = attachemntWarning;
              batchLog.transactions = transactions;
              // eslint-disable-next-line no-await-in-loop
              await insertLog(successCases, failedCases, startT, null, batchLog, 'full', false, 'zzother');
            }
          }
        }
      } else {
        // _.forEach(typeIDs, (id, index) => {
        // eslint-disable-next-line no-restricted-syntax
        for (const [index] of typeIDs.entries()) {
          if ((index + 1) % numOfBatch === 0 && (index + 1) >= numOfBatch) {
            // eslint-disable-next-line no-await-in-loop
            await migrateBatch(type, typeIDs[index + 1 - numOfBatch], typeIDs[`${index}`]);
          }
        }
        if (typeIDs.length % numOfBatch > 0) {
          // eslint-disable-next-line no-await-in-loop
          await migrateBatch(type,
            typeIDs[typeIDs.length - (typeIDs.length % numOfBatch)],
            typeIDs[typeIDs.length - 1]);
        }
      }
    }
  }
  if (lastEndType !== 'zzzunexpect') {
    const current = moment.utc(new Date()).add(8, 'hour').format('YYYYMMDDHHmm');
    if (current < endTime) {
      await handleUnexpect();
    }
  }
  // return p;
};


const migrateDeltaDelete = async (deletedIds) => {
  let delectedCnt = 0;
  // eslint-disable-next-line no-restricted-syntax
  for (const deletedId of deletedIds) {
    try {
      const collection = getCollectionNameById(deletedId);
      const type = getTypeByCollection(collection);
      const model = getModelByType(type);

      // eslint-disable-next-line no-await-in-loop
      const existDoc = await getDocs(model, { id: deletedId }, {});

      if (existDoc.length > 0) {
      // eslint-disable-next-line no-await-in-loop
        const result = await removeDocs(model, { id: deletedId });
        if (result.deletedCount > 0) {
          const successCase = { id: deletedId, type };
          successCase.statement = 'delete';
          successCases = successCases.concat(successCase);
          delectedCnt += result.deletedCount;
        } else {
          const failCase = { id: deletedId, type };
          failCase.statement = 'delete';
          failCase.failedReason = 'Cannot delete';
          failedCases = failedCases.concat(failCase);
        }
      } else {
        const failCase = { id: deletedId, type };
        failCase.statement = 'delete';
        failCase.warningReason = 'ID not exists';
        failCase.isWarning = true;
        failedCases = failedCases.concat(failCase);
      }
    } catch (error) {
      const failCase = { id: deletedId, collection: 'none' };
      failCase.statement = 'delete';
      failCase.failedReason = error.message;
      failedCases = failedCases.concat(failCase);
    }
  }
  return delectedCnt;
};


const deltaMigrate = async () => {
  const {
    deltaFromDate, deltaToDate,
  } = systemConfig.batchSetting;

  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('Delta Migrate start....');
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('');
  const startT = new Date();
  ranRecordsQuantity = 0;
  totalRecordsQuantity = 0;
  successCases = [];
  failedCases = [];
  s3LogsDelta = [];
  const fromDate = new Date(new Date(deltaFromDate).toLocaleDateString()).getTime();
  const toDate = new Date(new Date(`${deltaToDate}T23:59:59.999Z`).toLocaleDateString()).getTime();
  const result = await getAllDeltaDocs(fromDate, toDate);
  const needMigrateRecords = dataTypeStatistics(result.rows, true);

  const allTypeID = Object.entries(needMigrateRecords);

  _.forEach(allTypeID, (IDs) => {
    printLogWithTime(`Total count of docType - ${IDs[0]}: ${IDs[1].length}`);
    totalRecordsQuantity += IDs[1].length;
  });

  const deletedRecord = await getDoc('transactionDeleted');
  let deletedIds = [];

  _.forEach(deletedRecord.jsonList, (deletedRecords, key) => {
    if (key >= deltaFromDate && key <= deltaToDate) {
      deletedIds = deletedIds.concat(deletedRecords);
    }
  });

  printLogWithTime(`Total count of json deleted - ${deletedIds.length}`);
  totalRecordsQuantity += deletedIds.length;

  printLogWithTime(`Total record count: ${totalRecordsQuantity}`);

  await migrateDeltaRecords(needMigrateRecords);

  const deletedIdsCnt = await migrateDeltaDelete(deletedIds);


  const batchNumber = await getNextID('dataMigrationLog');

  const batchLog = {};
  let attachmentSuccess = 0;
  let attachmentFailed = 0;
  let attachemntWarning = 0;
  const transactions = [];
  _.forEach(s3LogsDelta, (log) => {
    attachmentSuccess += log.attachmentSuccess;
    attachmentFailed += log.attachmentFailed;
    attachemntWarning += log.attachemntWarning;
    transactions.push(log.transactions[0]);
  });
  batchLog.attachmentSuccess = attachmentSuccess;
  batchLog.attachmentFailed = attachmentFailed;
  batchLog.attachemntWarning = attachemntWarning;
  batchLog.transactions = transactions;
  await insertLog(successCases, failedCases, startT, batchNumber, batchLog, 'delta',
    { toDelete: deletedIds.length, deleted: deletedIdsCnt }, 'zzzzDelta');

  printLogWithTime('');

  printLogWithTime('Requerying how many records are there in couchbase');
  const allResult = await getAllDocsByType(null, null);
  const allRecords = dataTypeStatistics(allResult.rows, false);
  const allTypeIDFinal = Object.entries(allRecords);
  totalRecordsQuantity = 0;
  _.forEach(allTypeIDFinal, (IDs) => {
    printLogWithTime(`Total count of docType - ${IDs[0]}: ${IDs[1].length}`);
    totalRecordsQuantity += IDs[1].length;
  });

  printLogWithTime(`Total record count: ${totalRecordsQuantity}`);
};

const dataMigrate = async () => {
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('Data Extraction Started');
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime(' ');
  // const { isDebugMode, runImportMaster } = systemConfig.debugSetting;
  const { runDelta } = systemConfig.batchSetting;

  if (runDelta) {
    await deltaMigrate();
  } else {
    runDate = moment.utc(new Date()).add(8, 'hour').format('YYYY-MM-DD');
    endTime = moment(new Date(`${runDate} ${startTime}`)).add(duration, 'hour').format('YYYYMMDDHHmm');
    const { numOfBatch } = systemConfig.batchSetting;
    if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.rerunFull) {
      await removeDocs(migrateStatusModel, {});
      await removeDocs(dataMigrationUnexpectCaseModel, {});
    }
    /*
    if (isDebugMode) {
      if (runImportMaster) {
        await masterDataImport();
      }
    } else {
      const masterDataStatus = await getDocs(migrateStatusModel,
        { id: 'ImportMasterData' }, { id: 'desc' });
      if (masterDataStatus.length < 1) {
        await masterDataImport();
      }
    }
    */

    const lastMigrateStatus = await getDocs(migrateStatusModel,
      { id: { $ne: 'ImportMasterData' } }, { id: 'desc' });
    let lastEndType;
    let lastEndID;
    if (lastMigrateStatus.length > 0) {
    // eslint-disable-next-line no-underscore-dangle
      lastEndType = lastMigrateStatus[0]._doc.lastEndType;
      // eslint-disable-next-line no-underscore-dangle
      lastEndID = lastMigrateStatus[0]._doc.lastEndID;
    }

    if (systemConfig.debugSetting.isDebugMode && systemConfig.debugSetting.showMorelog) {
      printLogWithTime('Querying how many records are there in couchbase');
    }
    // const result = await getAllDocs();
    const result = await getAllDocsByType(lastEndType, lastEndID);
    let resultForOther;
    // const result = await getAllDocsContentByType('["agentXX","cust"]');
    // await updToSG('migrateDataIDSBackup', result);
    ranRecordsQuantity = 0;
    totalRecordsQuantity = 0;
    if (lastEndID) {
      result.rows.splice(0, 1);
      if (lastEndType !== 'zzother' && lastEndType !== 'zzzunexpect') {
        resultForOther = await getAllDocsByType(null, null);
      }
    }
    const needMigrateRecords = dataTypeStatistics(result.rows, false, resultForOther);
    const allTypeID = Object.entries(needMigrateRecords);

    _.forEach(allTypeID, (IDs) => {
      printLogWithTime(`Total count of docType - ${IDs[0]}: ${IDs[1].length}`);
      totalRecordsQuantity += IDs[1].length;
    });

    if (lastEndType !== 'zzzunexpect') {
      const unexpectDocs = await getDocs(dataMigrationUnexpectCaseModel,
        { id: 'CannotGetDocFromView' }, { id: 'desc' });
      if (unexpectDocs.length > 0) {
      // eslint-disable-next-line no-underscore-dangle
        const unexpect = unexpectDocs[0]._doc.unexpectIds;
        if (unexpect.length > 0) {
          printLogWithTime(`Total count of special handle cases: ${unexpect.length}`);
          totalRecordsQuantity += unexpect.length;
        }
      }
    }
    printLogWithTime(`Total record count: ${totalRecordsQuantity}`);

    // migrate log for contentious the batch
    const migrateStatusNumber = await getNextID('migrateStatus');
    migrateStatus.id = migrateStatusNumber;
    migrateStatus.lastEndType = lastEndType;
    migrateStatus.lastEndID = lastEndID;
    migrateStatus.TotalRecord = ranRecordsQuantity;
    migrateStatus.runDate = moment(new Date()).format('YYYY-MM-DD');
    migrateStatus.startTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS');
    await insertRecord(migrateStatusModel, migrateStatus);

    await migrateRecords(needMigrateRecords, Number(numOfBatch), lastEndType);

    migrateStatus.done = true;
    await findOneAndUpdate(migrateStatusModel, { id: migrateStatus.id }, migrateStatus);


    const checkMigrateStatus = await getDocs(migrateStatusModel,
      { id: { $ne: 'ImportMasterData' } }, { id: 'desc' });
    if (checkMigrateStatus.length > 0) {
    // eslint-disable-next-line no-underscore-dangle
      const endType = checkMigrateStatus[0]._doc.lastEndType;
      // eslint-disable-next-line no-underscore-dangle
      const endID = checkMigrateStatus[0]._doc.lastEndID;

      const result1 = await getAllDocsByType(endType, endID);
      if (result1.rows.length === 0) {
        printLogWithTime('Requerying how many records are there in couchbase');
        const allResult = await getAllDocsByType(null, null);
        const allRecords = dataTypeStatistics(allResult.rows, false);
        const allTypeIDFinal = Object.entries(allRecords);
        totalRecordsQuantity = 0;
        _.forEach(allTypeIDFinal, (IDs) => {
          printLogWithTime(`Total count of docType - ${IDs[0]}: ${IDs[1].length}`);
          totalRecordsQuantity += IDs[1].length;
        });

        printLogWithTime(`Total record count: ${totalRecordsQuantity}`);
      }
    }
  }
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('Data Extraction Ended');
  printLogWithTime('////////////////////////////////////////////////////////////');
};

export default {
  dataMigrate,
};
