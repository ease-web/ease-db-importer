import * as _ from 'lodash';
import systemConfig from '../config/config.json';
import {
  insertRecord, replaceDot,
} from '../util/documentDB';
import printLogWithTime from '../util/log';
import testDataModel from '../models/testData';
import masterDataModel from '../models/masterData';
import migrateStatusModel from '../models/migrateStatus';

const fs = require('fs');
const readline = require('readline');
const moment = require('moment');
const FileUtils = require('../util/fileUtils');

const dataImport = () => new Promise(() => '');

const getContentType = (fileType) => {
  if (fileType.toLowerCase() === 'png') {
    return 'image/png';
  } if (fileType.toLowerCase() === 'pdf') {
    return 'application/pdf';
  } if (fileType.toLowerCase() === 'jpg') {
    return 'image/jpeg';
  }
  return '';
};

const getJson = (attachments) => new Promise((resolve) => {
  const {
    masterJsonPath,
  } = systemConfig.batchSetting;

  let failedCnt = 0;

  const jsons = [];
  const jsonFile = fs.createReadStream('./masterData/json.txt');
  const jsonReadline = readline.createInterface({
    input: jsonFile,
  });

  jsonReadline.on('line', (line) => {
    try {
      const fileName = line.trim();
      const result = fs.readFileSync(`${masterJsonPath}/${fileName}`);

      const key = fileName.substring(0, fileName.lastIndexOf('.'));
      const attach = {};
      _.forEach(attachments, (attachment) => {
        if (key === attachment.docId) {
          const fileType = attachment.fileName.substring(attachment.fileName.lastIndexOf('.') + 1);
          attach[attachment.attachmentId] = {
            content_type: getContentType(fileType),
            length: attachment.dataLength,
            key: `${attachment.docId}-${attachment.attachmentId}`,
          };
        }
      });
      const jsonData = JSON.parse(result);
      jsonData.attachments = attach;
      jsonData.rev = 1;
      jsons.push({ line, jsonData: replaceDot(jsonData) });
    } catch (e) {
      failedCnt += 1;
      printLogWithTime(e);
    }
  })
    .on('close', () => {
      resolve({ jsons, failedCnt });
    });
});

const getAttachment = () => new Promise((resolve) => {
  const {
    masterAttachmentPath,
  } = systemConfig.batchSetting;

  const attachments = [];
  const attachFile = fs.createReadStream('./masterData/attachment.txt');
  const attachReadline = readline.createInterface({
    input: attachFile,
  });
  let failedCnt = 0;

  attachReadline.on('line', (line) => {
    try {
      // eslint-disable-next-line no-tabs
      const fileInfo = line.trim().toString().split('	');

      const data = fs.readFileSync(`${masterAttachmentPath}/${fileInfo[2].trim()}`);
      const bufferData = Buffer.from(data, 'binary').toString('base64');

      attachments.push({
        docId: fileInfo[0],
        attachmentId: fileInfo[1],
        data: bufferData,
        fileType: '',
        masterData: true,
        fileName: fileInfo[2].trim(),
        dataLength: data.length,
      });
    } catch (e) {
      failedCnt += 1;
      printLogWithTime(e);
    }
  })
    .on('close', () => {
      resolve({ attachments, failedCnt });
    });
});

const masterDataImport = async () => {
  const {
    isDebugMode, importToTestTable,
  } = systemConfig.debugSetting;

  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('Upload master attachments start....');
  printLogWithTime('////////////////////////////////////////////////////////////');
  const fileUtil = new FileUtils('AWS-S3');
  const fileInstance = fileUtil.getInstance();
  const initSuccess = fileInstance.init();

  let attachResult = {};
  let attachSuccessCnt = 0;
  let attachFailedCnt = 0;
  if (initSuccess) {
    attachResult = await getAttachment();
    const failedCases = await fileInstance.uploadBase64Batch(attachResult.attachments);
    if (attachResult.attachments) {
      attachSuccessCnt = attachResult.attachments.length - (failedCases ? failedCases.length : 0);
      attachFailedCnt = attachResult.failedCnt + (failedCases ? failedCases.length : 0);
    }
  }

  printLogWithTime('');
  printLogWithTime(`Upload master attachments done. ${attachSuccessCnt} success, ${attachFailedCnt} failed.`);
  printLogWithTime('');
  printLogWithTime('////////////////////////////////////////////////////////////');
  printLogWithTime('Upload master jsons start....');
  printLogWithTime('////////////////////////////////////////////////////////////');
  let successJson = 0;
  let failedJson = 0;
  const jsonResult = await getJson(attachResult.attachments);
  failedJson = jsonResult.failedCnt;
  // eslint-disable-next-line no-restricted-syntax
  for (const json of jsonResult.jsons) {
    try {
      if (importToTestTable && isDebugMode) {
      // eslint-disable-next-line no-await-in-loop
        await insertRecord(testDataModel, json.jsonData);
      } else {
      // eslint-disable-next-line no-await-in-loop
        await insertRecord(masterDataModel, json.jsonData);
      }
      successJson += 1;
    } catch (e) {
      failedJson += 1;
      printLogWithTime(e);
    }
  }

  const migrateStatus = {};
  migrateStatus.id = 'ImportMasterData';
  migrateStatus.Status = 'Done';
  migrateStatus.endDateTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS');
  await insertRecord(migrateStatusModel, migrateStatus);

  printLogWithTime('');
  printLogWithTime(`Insert master jsons done. ${successJson} success, ${failedJson} failed.`);
  printLogWithTime('');
};

export {
  dataImport, masterDataImport, getJson,
};
