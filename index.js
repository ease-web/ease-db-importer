import coubasePreCheck from './preCheck/couchbase';
import documentDBCheck from './preCheck/documentDB';
import s3PreCheck from './preCheck/s3';

import dataMigrateJob from './jobs/dataMigrate';
import config from './config/config.json';
import runSchedule from './jobs/runSchedule';

import printLogWithTime from './util/log';

const preCheck = async () => {
  try {
    const isConnectCouchbase = await coubasePreCheck.checkCouchbaseConnection();
    const isConnectDocumentDB = await documentDBCheck.checkDocumentConnection(true);

    if (isConnectDocumentDB && isConnectDocumentDB !== true) {
      printLogWithTime('Connect to AWS DocumentDB - Failed');
    } else {
      printLogWithTime('Connect to AWS DocumentDB - OK');
    }
    const isConnectS3 = await s3PreCheck.checkS3Connection();
    if (isConnectCouchbase === true && isConnectDocumentDB === true && isConnectS3 === true) {
      return true;
    }
  } catch (error) {
    printLogWithTime(`Connect to AWS DocumentDB - Failed ${error}`);
  }
  return false;
};

const start = async () => {
  const preCheckResult = await preCheck();
  const {
    startTime, fromDate, toDate, duration, numOfBatch,
  } = config.batchSetting;

  if (preCheckResult) {
    printLogWithTime('');
    printLogWithTime('***************************************');
    printLogWithTime('Data Migration is ready');
    printLogWithTime('');
    printLogWithTime(`Start Date        : ${fromDate}`);
    printLogWithTime(`End Date          : ${toDate}`);
    printLogWithTime(`Start Time        : ${startTime}`);
    printLogWithTime(`Duration          : ${duration}`);
    printLogWithTime(`Number of Batch   : ${numOfBatch}`);
    printLogWithTime('***************************************');
    printLogWithTime('');

    if (config.debugSetting.runAsStartServer && config.debugSetting.isDebugMode) {
      await dataMigrateJob.dataMigrate();
    } else {
      runSchedule.startSchedule();
    }
  } else {
    process.exit(1);
  }
};

start();
