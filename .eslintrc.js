module.exports = {
  extends: [
    "airbnb-base",
    "plugin:security/recommended"
  ],
  plugins: [
    "security"
  ],
  "rules": {
    "linebreak-style":[0,"error", "windows"]
  }
};