
import * as documentDB from './util/documentDB';
import migrateStatusModel from './models/migrateStatus';
import documentDBCheck from './preCheck/documentDB';
import printLogWithTime from './util/log';
import systemConfig from './config/config.json';

const cp = require('child_process');
const path = require('path');
const moment = require('moment');

const startJob = async () => {
  const maxBatchInterval = systemConfig.batchSetting.timeout;
  // const { startTime } = systemConfig.batchSetting;
  // const { duration } = systemConfig.batchSetting;

  await documentDBCheck.checkDocumentConnection(false);
  const migrateStatusCntBeforeRun = await documentDB.searchCnt(migrateStatusModel, {});

  const child = cp.fork(path.join(__dirname, './index.js'), []);

  let runingIDs = [];
  let runingType = '';
  let done = false;
  setInterval(async () => {
    // const runDate = moment(new Date()).format('YYYY-MM-DD');
    // const batchStartTime = moment(new Date(`${runDate} ${startTime}`))
    // .format('YYYYMMDDHHmmss.SSS');
    // const batchEndTime = moment(new Date(`${runDate} ${startTime}`))
    // .add(duration, 'hour').format('YYYYMMDDHHmmss.SSS');
    // const currentTime = moment(new Date()).format('YYYYMMDDHHmmss.SSS');

    // printLogWithTime(`batchStartTime = ${batchStartTime}`);
    // printLogWithTime(`batchEndTime = ${batchEndTime}`);
    // printLogWithTime(`currentTime = ${currentTime}`);

    // printLogWithTime(`currentTime > batchStartTime = ${currentTime > batchStartTime}`);
    // printLogWithTime(`currentTime < batchEndTime = ${currentTime < batchEndTime}`);

    // if (currentTime > batchStartTime) {
    let endTime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss.SSS');
    const lastMigrateStatus = await documentDB.getDocs(migrateStatusModel,
      { id: { $ne: 'ImportMasterData' } }, { id: 'desc' });

    if (lastMigrateStatus.length > migrateStatusCntBeforeRun) {
      // eslint-disable-next-line no-underscore-dangle
      if (lastMigrateStatus[0]._doc.endTime) {
        // eslint-disable-next-line no-underscore-dangle
        endTime = lastMigrateStatus[0]._doc.endTime;
      } else {
        // eslint-disable-next-line no-underscore-dangle
        endTime = lastMigrateStatus[0]._doc.startTime;
      }
      // eslint-disable-next-line no-underscore-dangle
      if (lastMigrateStatus[0]._doc.runingIDs) {
        // eslint-disable-next-line no-underscore-dangle
        runingIDs = lastMigrateStatus[0]._doc.runingIDs;
        // eslint-disable-next-line no-underscore-dangle
        runingType = lastMigrateStatus[0]._doc.runingType;
      }

      // eslint-disable-next-line no-underscore-dangle
      if (lastMigrateStatus[0]._doc.done) {
        // eslint-disable-next-line no-underscore-dangle
        done = lastMigrateStatus[0]._doc.done;
      }


      const killTime = moment(new Date()).add(-maxBatchInterval, 'minute').format('YYYY-MM-DD HH:mm:ss.SSS');
      // printLogWithTime(`endTime = ${endTime}`);
      // printLogWithTime(`killTime = ${killTime}`);
      if (killTime > endTime && !done) {
        child.kill();
        if (runingIDs.length > 0) {
          const model = documentDB.getModelByType(runingType);
          await documentDB.removeDocs(model, { id: { $in: runingIDs } });
        }
        printLogWithTime('Error - No response from destination, job killed.');
        process.exit();
      }
    }
    // }
  }, 60000);


  // Listen for messages from the child
  // child.on('message', (data) => printLogWithTime(data));
};


startJob();
