import axios from 'axios';
import config from '../config/config.json';
import printLogWithTime from '../util/log';

const checkCouchbaseConnection = async () => {
  printLogWithTime('========== 1 - Connect to Couchbase Sync Gateway==========');
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}`;
  printLogWithTime(`Gateway URL: ${baseURL}`);
  printLogWithTime(`Gateway bucket: ${config.couchbaseSyncGateway.name}`);
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    printLogWithTime('Connect to Couchbase Sync Gateway - OK');
    printLogWithTime('');
    return true;
  }
  // eslint-disable-next-line no-console
  console.error('Connect to Couchbase Sync Gateway - FAILED');
  console.error(result);
  printLogWithTime('');
  return false;
};

export default {
  checkCouchbaseConnection,
};
