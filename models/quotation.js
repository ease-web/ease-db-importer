const mongoose = require('mongoose');

const quotationModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'quotation', strict: false, minimize: false });

module.exports = mongoose.model('quotation', quotationModel);
