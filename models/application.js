const mongoose = require('mongoose');

const applicationModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'application', strict: false, minimize: false });

module.exports = mongoose.model('application', applicationModel);
