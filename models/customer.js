const mongoose = require('mongoose');

const customerModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'customer', strict: false, minimize: false });

module.exports = mongoose.model('customer', customerModel);
