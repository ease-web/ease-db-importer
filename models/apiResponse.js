const mongoose = require('mongoose');

const apiResponseModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'apiResponse', strict: false, minimize: false });

module.exports = mongoose.model('apiResponse', apiResponseModel);
