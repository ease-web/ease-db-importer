const mongoose = require('mongoose');

const testDataModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'testDataMigrateXXXX', strict: false, minimize: false });

module.exports = mongoose.model('testDataMigrateXXXX', testDataModel);
