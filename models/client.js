const mongoose = require('mongoose');

const clientMigratedModel = new mongoose.Schema({
  id: {
    type: String,
  },
  addrBlock: {
    type: String,
  },
  addrCity: {
    type: String,
  },
  addrCountry: {
    type: String,
  },
  addrEstate: {
    type: String,
  },
  addrStreet: {
    type: String,
  },
  age: {
    type: Number,
  },
  agentCode: {
    type: String,
  },
  agentId: {
    type: String,
  },
  allowance: {
    type: Number,
  },
  applicationCount: {
    type: Number,
  },
  bundle: {
    type: Object,
  },
  channel: {
    type: String,
  },
  cid: {
    type: String,
  },
  compCode: {
    type: String,
  },
  dealerGroup: {
    type: Object,
  },
  dob: {
    type: String,
  },
  dependants: {
    type: Array,
  },
  education: {
    type: String,
  },
  email: {
    type: String,
  },
  employStatus: {
    type: String,
  },
  firstName: {
    type: String,
  },
  fnaRecordIdArray: {
    type: String,
  },
  fullName: {
    type: String,
  },
  gender: {
    type: String,
  },
  hanyuPinyinName: {
    type: String,
  },
  hasIFAST: {
    type: String,
  },
  hasUT: {
    type: String,
  },
  idCardNo: {
    type: String,
  },
  industry: {
    type: String,
  },
  initial: {
    type: String,
  },
  isSmoker: {
    type: String,
  },
  language: {
    type: String,
  },
  lastName: {
    type: String,
  },
  lstChgDate: {
    type: Number,
  },
  marital: {
    type: String,
  },
  mobileCountryCode: {
    type: String,
  },
  mobileNo: {
    type: String,
  },
  nameOrder: {
    type: String,
  },
  nationality: {
    type: String,
  },
  nearAge: {
    type: Number,
  },
  occupation: {
    type: String,
  },
  organization: {
    type: String,
  },
  organizationCountry: {
    type: String,
  },
  othName: {
    type: String,
  },
  otherMobileCountryCode: {
    type: String,
  },
  otherNo: {
    type: String,
  },
  photo: {
    type: Object,
  },
  postalCode: {
    type: String,
  },
  referrals: {
    type: String,
  },
  residenceCountry: {
    type: String,
  },
  title: {
    type: String,
  },
  type: {
    type: String,
  },
  unitNum: {
    type: String,
  },
  haveSignDoc: {
    type: Object,
  },
  lastUpdateDate: {
    type: String,
  },
  prStatus: {
    type: String,
  },
  newCid: {
    type: String,
  },
  otherCityOfResidence: {
    type: String,
  },
  trustedIndividuals: {
    type: Object,
  },
  passOther: {
    type: String,
  },
  languageOther: {
    type: String,
  },
  occupationOther: {
    type: String,
  },
  pass: {
    type: String,
  },
  relationshipOther: {
    type: String,
  },
  employStatusOther: {
    type: String,
  },
  idDocType: {
    type: String,
  },
  idDocTypeOther: {
    type: String,
  },
  otherResidenceCity: {
    type: String,
  },
  relationship: {
    type: String,
  },
  residenceCity: {
    type: String,
  },
  passExpDate: {
    type: Number,
  },
  attachments: {
    type: Object,
  },
  isSameAddr: {
    type: String,
  },
  branchInfo: {
    type: Object,
  },
  isValid: {
    type: Boolean,
  },
  isBackup: {
    type: Boolean,
  },
}, { collection: 'clientMigrated' });

module.exports = mongoose.model('clientMigrated', clientMigratedModel);
