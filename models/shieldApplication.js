const mongoose = require('mongoose');

const shieldApplicationModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'shieldApplication', strict: false, minimize: false });

module.exports = mongoose.model('shieldApplication', shieldApplicationModel);
