const mongoose = require('mongoose');

const fnaFeModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'fnaFe', strict: false, minimize: false });

module.exports = mongoose.model('fnaFe', fnaFeModel);
