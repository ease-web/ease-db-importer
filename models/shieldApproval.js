const mongoose = require('mongoose');

const shieldApprovalModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'shieldApproval', strict: false, minimize: false });

module.exports = mongoose.model('shieldApproval', shieldApprovalModel);
