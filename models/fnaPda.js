const mongoose = require('mongoose');

const fnaPdaModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'fnaPda', strict: false, minimize: false });

module.exports = mongoose.model('fnaPda', fnaPdaModel);
