const mongoose = require('mongoose');

const dataMigrationUnexpectCaseModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'dataMigrationUnexpectCase', strict: false, minimize: false });

module.exports = mongoose.model('dataMigrationUnexpectCase', dataMigrationUnexpectCaseModel);
