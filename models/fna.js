const mongoose = require('mongoose');

const fnaModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'fna', strict: false, minimize: false });

module.exports = mongoose.model('fna', fnaModel);
