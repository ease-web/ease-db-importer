const mongoose = require('mongoose');

const agentExtraInfoModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'agentExtraInfo', strict: false, minimize: false });

module.exports = mongoose.model('agentExtraInfo', agentExtraInfoModel);
