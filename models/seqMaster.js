const mongoose = require('mongoose');

const seqMasterModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'seqMaster', strict: false, minimize: false });

module.exports = mongoose.model('seqMaster', seqMasterModel);
