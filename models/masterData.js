const mongoose = require('mongoose');

const masterDataModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'masterData', strict: false, minimize: false });

module.exports = mongoose.model('masterData', masterDataModel);
