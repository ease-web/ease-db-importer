const mongoose = require('mongoose');

const dataExtractionErrorLogModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'dataExtractionErrorLog', strict: false, minimize: false });

module.exports = mongoose.model('dataExtractionErrorLog', dataExtractionErrorLogModel);
