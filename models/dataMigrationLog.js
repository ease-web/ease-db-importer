const mongoose = require('mongoose');

const dataMigrationLogModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'dataMigrationLog', strict: false, minimize: false });

module.exports = mongoose.model('dataMigrationLog', dataMigrationLogModel);
