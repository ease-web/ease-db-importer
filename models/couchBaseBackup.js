const mongoose = require('mongoose');

const couchBaseBackupModel = new mongoose.Schema({

}, { collection: 'couchBaseBackup', strict: false, minimize: false });

module.exports = mongoose.model('couchBaseBackup', couchBaseBackupModel);
