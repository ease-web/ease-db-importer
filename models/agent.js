const mongoose = require('mongoose');

const agentModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'agent', strict: false, minimize: false });

module.exports = mongoose.model('agent', agentModel);
