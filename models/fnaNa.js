const mongoose = require('mongoose');

const fnaNaModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'fnaNa', strict: false, minimize: false });

module.exports = mongoose.model('fnaNa', fnaNaModel);
