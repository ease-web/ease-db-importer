const mongoose = require('mongoose');

const approvalModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'approval', strict: false, minimize: false });

module.exports = mongoose.model('approval', approvalModel);
