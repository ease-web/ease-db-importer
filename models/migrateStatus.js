const mongoose = require('mongoose');

const migrateStatusModel = new mongoose.Schema({
  id: {
    type: String,
  },
}, { collection: 'dataMigrationStatus', strict: false, minimize: false });

module.exports = mongoose.model('migrateStatus', migrateStatusModel);
