const mongoose = require('mongoose');

const { Schema } = mongoose;

const iDSchema = new Schema({
  IDNO: Number,
  TableName: String,
},
{
  collection: 'dataMigrationId',
});
module.exports = mongoose.model('ModelIDS', iDSchema);
