## Contents

* [Setup](#setup)
* [Available Scripts](#available-scripts)
  * [Start](#Development-start)
  * [ESlint Checking](#ESlint-Checking)
  * [Detect Vulnerable Dependencies](#Detect-vulnerable-dependencies)

## Setup
Install dependencies
```
npm install
```
Or using yarn
```
yarn
```

## Available Scripts
### Development start
Run the project in development mode
```
npm run start
```
Or
```
yarn start
```

### ESlint Checking
Run [eslint](https://github.com/eslint/eslint) utility to check whether the code format is correct.
```
npm run eslintChecking
```
Or
```
yarn eslintChecking
```

### Detect vulnerable dependencies
Use npm audit tool to check vulnerable dependencies in cause bring vulnerable dependencies into our project
```
npm run vulnerabilitiesChecking
```
Or
```
yarn vulnerabilitiesChecking
```