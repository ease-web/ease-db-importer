import * as _ from 'lodash';
import axios from 'axios';
import config from '../config/config.json';
import printLogWithTime from './log';

const http = require('http');

const getAllDocs = async () => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}/_all_docs`;
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getDoc = async (docId) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}/${docId}`;
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getAllDocsByType = async (lastEndType, lastEndID) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  let url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllTypesAndIDs?stale=false&descending=false`;
  if (lastEndID) {
    if (lastEndType === null || lastEndType === 'null') {
      url = `${url}&startkey=[null, "${lastEndID}"]`;
    } else {
      url = `${url}&startkey=["${lastEndType}", "${lastEndID}"]`;
    }
  }
  const { importSpecifiedDocIDs, isDebugMode } = config.debugSetting;
  if (importSpecifiedDocIDs && isDebugMode) {
    url = `${url}&keys=[[${importSpecifiedDocIDs}]]`;
  }

  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getAllDocsContentByType = async (type) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  let url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllDocsContentByType?stale=false`;
  if (type) {
    url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllDocsContentByType?stale=false&keys=${type}`;
  }
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getAllDocsContentByTypesAndIDs = async (type, start, end) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const newType = type === 'empty' ? '' : type;
  let url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllDocsContentByTypesAndIDs?stale=false&startkey=["${newType}","${start}"]&endkey=["${type}","${end}"]`;
  if (newType === 'null') {
    url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllDocsContentByTypesAndIDs?stale=false&startkey=[null,"${start}"]&endkey=[null,"${end}"]`;
  }
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getTypesAndIDsByRange = async (type, start, end) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const newType = type === 'empty' ? '' : type;
  let url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllTypesAndIDs?stale=false&startkey=["${newType}","${start}"]&endkey=["${type}","${end}"]`;
  if (newType === 'null') {
    url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllTypesAndIDs?stale=false&startkey=[null,"${start}"]&endkey=[null,"${end}"]`;
  }
  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getAttachment = async (id, file) => new Promise((resolve, reject) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;

  const url = `/${config.couchbaseSyncGateway.name}/${id}/${file}`;
  const startTime = new Date();
  axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
    responseType: 'arraybuffer',
  })
    .then((response) => {
      if (response && response.status && response.status === 200) {
        if (response.data) {
          const data = Buffer.from(response.data, 'binary').toString('base64');
          let dataLength = 0;
          try {
            dataLength = Math.round((response.data.length / 1048576) * 10000) / 10000;
          } catch (ex) {
            dataLength = 'error';
          }
          const elapsedTimeMs = new Date() - startTime;
          if (config.debugSetting.isDebugMode && config.debugSetting.showMorelog) {
            printLogWithTime(`Get ${id}-${file} from Couchbase: ${dataLength}MB: Elapsed ${elapsedTimeMs}ms`);
          }
          resolve({
            success: true,
            data,
          });
        } else {
          const elapsedTimeMs = new Date() - startTime;
          printLogWithTime(`Failed get ${id}-${file} from Couchbase: Elapsed ${elapsedTimeMs}ms`);
          // eslint-disable-next-line prefer-promise-reject-errors
          reject({ message: `Error to get ${id}/${file} from Couchbase` });
        }
      } else {
        printLogWithTime(`Error to get ${id}-${file} from Couchbase`);
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ message: `Error to get ${id}/${file} from Couchbase` });
      }
    })
    .catch((error) => {
      printLogWithTime(`Warning - Failed to get ${id}-${file} from Couchbase`);
      reject(error);
    });
});

const getAttachmentByBase64 = async (docId, attachmentId) => new Promise((resolve, reject) => {
  let host = config.couchbaseSyncGateway.path;
  if (host.indexOf('://') > -1) {
    host = host.substring(host.indexOf('://') + 3);
  }
  const url = `/${config.couchbaseSyncGateway.name}/${docId}/${attachmentId}`;
  const options = {
    hostname: host,
    path: url,
    method: 'GET',
  };
  if (config.couchbaseSyncGateway.port) {
    options.port = config.couchbaseSyncGateway.port;
  }
  if (config.couchbaseSyncGateway.userName && config.couchbaseSyncGateway.password) {
    options.auth = `${config.couchbaseSyncGateway.userName}:${config.couchbaseSyncGateway.password}`;
  }

  const startTime = new Date();
  const req = http.request(options, (res) => {
    let data = '';
    res.setEncoding('binary');
    res.on('data', (chunk) => {
      data += chunk;
    });
    res.on('end', () => {
      const elapsedTimeMs = new Date() - startTime;
      let dataLength = 0;
      try {
        dataLength = Math.round((data.length / 1048576) * 10000) / 10000;
      } catch (ex) {
        dataLength = 'error';
      }
      if (config.debugSetting.isDebugMode && config.debugSetting.showMorelog) {
        printLogWithTime(`Get ${docId}-${attachmentId} from Couchbase: ${dataLength}MB: Elapsed ${elapsedTimeMs}ms`);
      }

      if (data) {
        data = Buffer.from(data, 'binary').toString('base64');
        resolve({
          success: true,
          data,
        });
      } else {
        // eslint-disable-next-line prefer-promise-reject-errors
        reject({ message: `Error to get ${docId}/${attachmentId} from Couchbase` });
      }
    });
  }).on('error', (e) => {
    printLogWithTime(`Error returned from Couchbase Sync Gateway: ${e} - ${docId}/${attachmentId}`);
    reject(e);
  });
  req.end();
});

const updToSG = async (docId, data) => new Promise((resolve, reject) => {
  let host = config.couchbaseSyncGateway.path;
  if (host.indexOf('://') > -1) {
    host = host.substring(host.indexOf('://') + 3);
  }
  const url = `/${config.couchbaseSyncGateway.name}/${docId}`;
  const options = {
    hostname: host,
    path: url,
    method: 'PUT',
    headers: 'application/json',
  };
  if (config.couchbaseSyncGateway.port) {
    options.port = config.couchbaseSyncGateway.port;
  }
  if (config.couchbaseSyncGateway.userName && config.couchbaseSyncGateway.password) {
    options.auth = `${config.couchbaseSyncGateway.userName}:${config.couchbaseSyncGateway.password}`;
  }

  const req = http.request(options, (res) => {
    let resp = '';
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      resp += chunk;
    });
    res.on('end', () => {
      if (resp) {
        try {
          resp = JSON.parse(resp);
          // logger.log('updata to SG result:', resp);
          if (resp.ok && !resp.error) {
            resolve(resp);
          } else {
            printLogWithTime('update CB error:', resp);
            reject(resp.error);
          }
        } catch (e) {
          // calling error
          printLogWithTime('update CB error:', e, resp);
          reject(resp.error);
        }
      } else {
        reject(resp.error);
      }
    });
  }).on('error', (e) => {
    printLogWithTime('ERROR:: SG Error: ', e);
    reject(e);
  });
  if (data) {
    if (typeof data === 'object') {
      // eslint-disable-next-line no-param-reassign
      data.lstChgDate = (new Date()).getTime();
      // eslint-disable-next-line no-param-reassign
      data = JSON.stringify(data);
    }
    req.write(data);
  }
  req.end();
});

const getAllDeltaDocs = async (fromDate, toDate) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllTypesAndIDsByLstChgDate?descending=false&stale=false&startkey=[${fromDate}]&endkey=[${toDate}]`;

  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getAllDocsContentByID = async (id) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}/${id}`;

  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const getTypeByID = async (id) => {
  const baseURL = `${config.couchbaseSyncGateway.path}${config.couchbaseSyncGateway.port ? `:${config.couchbaseSyncGateway.port}` : ''}`;
  const url = `/${config.couchbaseSyncGateway.name}/_design/dataMigration/_view/getAllTypesAndIDsByID?stale=false&key=["${id}"]`;

  const result = await axios.request({
    baseURL,
    url,
    auth: {
      username: config.couchbaseSyncGateway.userName,
      password: config.couchbaseSyncGateway.password,
    },
  }).catch((error) => ({ error }));
  if (result && result.status && result.status === 200) {
    return result.data;
  }
  return result.error;
};

const dataTypeStatistics = (records, isDelta, resultForOther) => {
  // const { importMasterdataOnly, isDebugMode } = systemConfig.debugSetting;

  const needMigrateRecords = {};

  const custIDs = [];
  const agentIDs = [];
  const userExtIDs = [];
  const bundleIDs = [];
  const feIDs = [];
  const naIDs = [];
  const pdaIDs = [];
  const quotationIDs = [];
  const applicationIDs = [];
  const approvalIDs = [];
  const masterApplicationIDs = [];
  const masterApprovalIDs = [];
  const seqMasterIDs = [];
  const APIRESPONSEIDs = [];
  const BLOCKIDs = [];
  const PAPERIDs = [];
  const apiResponseIDs = [];
  const campaignIDs = [];
  const emailTemplateIDs = [];
  const layoutIDs = [];
  const menuItemIDs = [];
  const pdfTemplateIDs = [];
  const settingsIDs = [];
  const setupIDs = [];
  const templateIDs = [];
  const materialIDs = [];
  const fundIDs = [];
  const productIDs = [];
  const DATASYNCTRXLOGIDs = [];
  const emptyIDs = [];
  const nullIDs = [];
  const otherIDs = [];

  const excludeList = ['08_product_NPE_2', 'testingNPE'];

  _.forEach(records, (record) => {
    let exclude = false;
    _.forEach(excludeList, (item) => {
      if (item === record.id) {
        exclude = true;
      }
    });

    if (!exclude) {
      let type = record.key[0];
      if (isDelta) {
        type = record.value.docType;
      }
      switch (type) {
        case 'cust': custIDs.push(record.id); break;
        case 'agent': agentIDs.push(record.id); break;
        case 'userExt': userExtIDs.push(record.id); break;
        case 'bundle': bundleIDs.push(record.id); break;
        case 'fe': feIDs.push(record.id); break;
        case 'na': naIDs.push(record.id); break;
        case 'pda': pdaIDs.push(record.id); break;
        case 'quotation': quotationIDs.push(record.id); break;
        case 'application': applicationIDs.push(record.id); break;
        case 'approval': approvalIDs.push(record.id); break;
        case 'masterApplication': masterApplicationIDs.push(record.id); break;
        case 'masterApproval': masterApprovalIDs.push(record.id); break;
        case 'seq': seqMasterIDs.push(record.id); break;
        case 'API_RESPONSE': APIRESPONSEIDs.push(record.id); break;
        case 'BLOCK': BLOCKIDs.push(record.id); break;
        case 'PAPER': PAPERIDs.push(record.id); break;
        case 'apiResponse': apiResponseIDs.push(record.id); break;
        case 'campaign': campaignIDs.push(record.id); break;
        case 'email_template': emailTemplateIDs.push(record.id); break;
        case 'layout': layoutIDs.push(record.id); break;
        case 'menuItem': menuItemIDs.push(record.id); break;
        case 'pdfTemplate': pdfTemplateIDs.push(record.id); break;
        case 'settings': settingsIDs.push(record.id); break;
        case 'setup': setupIDs.push(record.id); break;
        case 'template': templateIDs.push(record.id); break;
        case 'material': materialIDs.push(record.id); break;
        case 'fund': fundIDs.push(record.id); break;
        case 'product': productIDs.push(record.id); break;
        case 'DATA_SYNC_TRX_LOG': DATASYNCTRXLOGIDs.push(record.id); break;
        case '': emptyIDs.push(record.id); break;
        case null:
        // if (record.id.substring(record.id.length - 4) === '-seq'
        // || record.id === 'agentNumberMap') {
        // seqMasterIDs.push(record.id);
        // } else {
          nullIDs.push(record.id);
          // }
          break;
        default: otherIDs.push(record.id); break;
      }
    }
  });

  // if (!importMasterdataOnly || !isDebugMode) {
  needMigrateRecords.cust = custIDs;
  needMigrateRecords.agent = agentIDs;
  needMigrateRecords.userExt = userExtIDs;
  needMigrateRecords.bundle = bundleIDs;
  needMigrateRecords.fe = feIDs;
  needMigrateRecords.na = naIDs;
  needMigrateRecords.pda = pdaIDs;
  needMigrateRecords.quotation = quotationIDs;
  needMigrateRecords.application = applicationIDs;
  needMigrateRecords.approval = approvalIDs;
  needMigrateRecords.masterApplication = masterApplicationIDs;
  needMigrateRecords.masterApproval = masterApprovalIDs;
  needMigrateRecords.seq = seqMasterIDs;
  needMigrateRecords.API_RESPONSE = APIRESPONSEIDs;
  // } else {
  needMigrateRecords.BLOCK = BLOCKIDs;
  needMigrateRecords.PAPER = PAPERIDs;
  needMigrateRecords.apiResponse = apiResponseIDs;
  needMigrateRecords.campaign = campaignIDs;
  needMigrateRecords.email_template = emailTemplateIDs;
  needMigrateRecords.layout = layoutIDs;
  needMigrateRecords.menuItem = menuItemIDs;
  needMigrateRecords.pdfTemplate = pdfTemplateIDs;
  needMigrateRecords.settings = settingsIDs;
  needMigrateRecords.setup = setupIDs;
  needMigrateRecords.template = templateIDs;
  needMigrateRecords.material = materialIDs;
  needMigrateRecords.fund = fundIDs;
  needMigrateRecords.product = productIDs;
  needMigrateRecords.DATA_SYNC_TRX_LOG = DATASYNCTRXLOGIDs;
  needMigrateRecords.empty = emptyIDs;
  needMigrateRecords.null = nullIDs;
  needMigrateRecords.other = otherIDs;
  // }
  if (resultForOther) {
    const otherRecards = dataTypeStatistics(resultForOther.rows, false, null);
    needMigrateRecords.other = otherRecards.other;
  }
  return needMigrateRecords;
};

const getCollectionNameById = (docId) => {
  let type = '';
  if (_.endsWith(docId, '_RLSSTATUS')) {
    type = 'apiResponse';
  } else if (docId.substring(0, 2) === '10' || docId.substring(0, 2) === '30') {
    type = 'approval';
  } else if (docId.substring(0, 2) === 'CP') {
    type = 'customer';
  } else if (docId.substring(0, 2) === 'FN') {
    if (_.endsWith(docId, '-FE')) type = 'fnaFe';
    else if (_.endsWith(docId, '-NA')) type = 'fnaNa';
    else if (_.endsWith(docId, '-PDA')) type = 'fnaPda';
    else type = 'fna';
  } else if (docId.substring(0, 2) === 'NB') {
    type = 'application';
  } else if (docId.substring(0, 2) === 'QU') {
    type = 'quotation';
  } else if (docId.substring(0, 2) === 'SA') {
    type = 'shieldApplication';
  } else if (docId.substring(0, 2) === 'SP') {
    type = 'shieldApproval';
  } else if (docId.substring(0, 2) === 'U_') {
    type = 'agent';
  } else if (docId.substring(0, 3) === 'UX_') {
    type = 'agentExtraInfo';
  // } else if (docId.substring(0, 6) === 'appid-') {
  //   type = 'appid';
  // } else if (docId.substring(0, 4) === 'AUD-') {
  //   type = 'aud';
  // } else if (docId.length === 52 || docId.length === 50) {
  //   type = 'dataSyncTransactionLog';
  } else if (_.endsWith(docId, '-seq') || _.eq(docId, 'agentNumberMap')) {
    type = 'seqMaster';
  } else {
    type = 'masterData';
  }
  printLogWithTime(`Collection name - ${type}`);
  return type;
};

export {
  getAllDocs, getDoc, getAllDocsByType, getAllDocsContentByType, getAllDocsContentByTypesAndIDs,
  getAttachment, getAttachmentByBase64, updToSG, getAllDeltaDocs, getAllDocsContentByID,
  getTypeByID, getTypesAndIDsByRange, dataTypeStatistics, getCollectionNameById,
};
