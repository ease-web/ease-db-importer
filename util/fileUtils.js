/* eslint-disable no-empty */
/* eslint-disable import/no-unresolved */
/* eslint-disable new-cap */
/* eslint-disable class-methods-use-this */
/* eslint-disable global-require */
import config from '../config/config.json';

const _ = require('lodash');

class fileUtils {
  constructor(fileSystem) {
    this.fileSystem = fileSystem;
  }

  getInstance() {
    let dao = '';
    if (_.eq(this.fileSystem, 'AWS-S3')) {
      const aws = require('./aws/s3');
      dao = new aws();
    }
    return dao;
  }

  // gothrough proxy
  setProxyEnv() {
    const HttpProxyAgent = require('https-proxy-agent');
    const proxyAgent = new HttpProxyAgent(
      process.env.aws_https_proxy || process.env.AWS_HTTPS_PROXY,
    );
    return proxyAgent;
  }

  calBase64FileSize(inBase64) {
    let base64 = _.replace(inBase64, '=', '');
    base64 = _.replace(base64, /^data:image\/\w+;base64,/, '');
    const strLength = _.size(base64);
    const fileSize = strLength - (strLength / 8) * 2;
    return fileSize;
  }

  getBucketNameById(docId) {
    let bucket = config.awsS3.transBucket;
    if (docId.substring(0, 2) === '10' || docId.substring(0, 2) === '30') {
    } else if (docId.substring(0, 2) === 'CP') {
    } else if (docId.substring(0, 2) === 'FN') {
    } else if (docId.substring(0, 2) === 'NB') {
    } else if (docId.substring(0, 2) === 'QU') {
    } else if (docId.substring(0, 2) === 'SA') {
    } else if (docId.substring(0, 2) === 'SP') {
    } else if (docId.substring(0, 2) === 'U_') {
    } else if (docId.substring(0, 3) === 'UX_') {
    // } else if (docId.substring(0, 6) === 'appid-') {
    // } else if (docId.substring(0, 4) === 'AUD-') {
    // } else if (docId.length === 52 || docId.length === 50) {
    } else if (_.endsWith(docId, '-seq') || _.eq(docId, 'agentNumberMap')) {
    } else {
      bucket = config.awsS3.masterBucket;
    }
    return bucket;
  }

  getFileKeyById(docId, attachent) {
    const fileKey = _.join([docId, attachent], '-');
    return fileKey;
  }
}


module.exports = fileUtils;
