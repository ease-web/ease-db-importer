import * as _ from 'lodash';

import customerModel from '../models/customer';
import agentModel from '../models/agent';
import agentExtraInfoModel from '../models/agentExtraInfo';
import fnaModel from '../models/fna';
import fnaFeModel from '../models/fnaFe';
import fnaNaModel from '../models/fnaNa';
import fnaPdaModel from '../models/fnaPda';
import quotationModel from '../models/quotation';
import applicationModel from '../models/application';
import approvalModel from '../models/approval';
import shieldApplicationModel from '../models/shieldApplication';
import shieldApprovalModel from '../models/shieldApproval';
import masterDataModel from '../models/masterData';
import seqMasterModel from '../models/seqMaster';
import testDataModel from '../models/testData';
import apiResponseModel from '../models/apiResponse';
import systemConfig from '../config/config.json';

import printLogWithTime from './log';
// eslint-disable-next-line no-unused-vars
import { getAttachmentByBase64, getAttachment } from './couchbase';

const FileUtils = require('./fileUtils');
const ModelIDS = require('../models/IDS');

const insertRecord = async (Collection, doc) => {
  const newDoc = new Collection(doc);
  const docOnDocumentDB = await newDoc.save();
  return docOnDocumentDB[`${'_id'}`];
};

const getDocByCondition = async (collection, condition) => {
  const doc = await collection.findOne(condition);
  return doc;
};

const getDocs = async (collection, condition, sort) => {
  const doc = await collection.find(condition).sort(sort);
  return doc;
};

const removeDocs = async (collection, condition) => {
  const doc = await collection.deleteMany(condition);
  return doc;
};


const findOneAndUpdate = async (collection, condition, updStr) => {
  const doc = await collection.findOneAndUpdate(condition,
    {
      $set:
      updStr,
    },
    { new: true });
  return doc;
};

const insertMany = (Model, modelArray) => new Promise((resolve, reject) => {
  Model.insertMany(modelArray, (err, docs) => {
    if (err) {
      reject(err);
    } else {
      resolve(docs);
    }
  });
});

const searchCnt = (modelObj, queryParams) => new Promise((resolve, reject) => {
  modelObj.countDocuments(queryParams, (err, count) => {
    if (err) {
      reject(err);
    } else {
      resolve(count);
    }
  });
});

const dbHelpGetSeq = (tableKey) => new Promise((resolve, reject) => {
  ModelIDS.findOneAndUpdate({ TableName: tableKey }, { $inc: { IDNO: 1 } },
    { new: true },
    (err, newRecord) => {
      if (err) {
        reject(err);
      } else {
        resolve(newRecord);
      }
    });
});
const saveRecord = (Model) => new Promise((resolve, reject) => {
  Model.save((err, res) => {
    if (err) {
      reject(err);
    } else {
      resolve(res);
      // console.log(`Res:${res}`);
    }
  });
});

const autoFillZero = (prev, num, n) => {
  // this function can not format 0 to 0000
  if (!num) {
    return num;
  } if (num.toString().length >= n) {
    return prev + num;
  }
  return prev + (Array(n).join(0) + num).slice(-n);
};

async function getNextIDFunc(tableName) {
  let returnID = 1;
  const cnt2 = await searchCnt(ModelIDS, { TableName: tableName });
  if (cnt2 > 0) {
    const ids = await dbHelpGetSeq(tableName);
    returnID = ids.IDNO;
  } else {
    const modelids = new ModelIDS({
      IDNO: returnID,
      TableName: tableName,
    });
    await saveRecord(modelids);
  }

  returnID = autoFillZero('', returnID, 5);


  return returnID;
}

const getNextID = (tableName) => new Promise((resolve) => {
  resolve(getNextIDFunc(tableName));
});

async function addAttachmentFunc(docs, type) {
  const fileUtil = new FileUtils('AWS-S3');
  const fileInstance = fileUtil.getInstance();
  const initSuccess = fileInstance.init();
  const batchLog = {};
  const s3Logs = [];
  let succesCnt = 0;
  let failedCnt = 0;
  let warningCnt = 0;
  const uploadDocs = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const doc of docs.rows) {
    const docLog = {};
    const docId = doc.id;
    docLog.docID = docId;
    const attachments = [];
    // eslint-disable-next-line no-underscore-dangle
    const attachs = doc.value._attachments;
    if (attachs) {
      const attachmentsArray = Object.entries(attachs);
      // eslint-disable-next-line no-restricted-syntax
      for (const attach of attachmentsArray) {
        const attachmentId = attach[0];
        const attachment = {};
        attachment.bucket = fileInstance.getBucketNameById(docId);
        attachment.name = attachmentId;
        attachment.key = `${docId}-${attachmentId}`;
        attachment.result = 'passed';
        // eslint-disable-next-line no-await-in-loop
        const result = await getAttachment(docId, attachmentId)
          // eslint-disable-next-line no-loop-func
          .catch((error) => {
            // failedCnt += 1;
            attachment.result = 'warning';
            attachment.warningReason = error.message;
            printLogWithTime(error.message);
          });
        if (result) {
          uploadDocs.push({
            docId,
            attachmentId,
            data: (result ? result.data : null),
            fileType: attach[1].content_type,
            type,
          });
        }


        /*
        if (result && initSuccess) {
          // eslint-disable-next-line no-await-in-loop
          fileInstance.uploadBase64(docId, attachmentId, result.data,
            attach[1].content_type, type)
            .catch((error) => {
              hasError = true;
              attachment.failedReason = error.message;
              printLogWithTime(error.message);
            });
          if (!hasError) {
            success = true;
            succesCnt += 1;
            attachment.result = 'passed';
          }
        }

        if (!success) {
          failedCnt += 1;
          attachment.result = 'failed';
        }
        */

        attachments.push(attachment);
      }
    }
    docLog.attachments = attachments;
    s3Logs.push(docLog);
  }
  let uploadResult = [];
  if (initSuccess) {
    uploadResult = await fileInstance.uploadBase64Batch(uploadDocs);
  }


  _.forEach(s3Logs, (tran, ind1) => {
    _.forEach(tran.attachments, (attach, ind2) => {
      _.forEach(uploadResult, (result) => {
        if (attach.key === result.key) {
          s3Logs[ind1].attachments[ind2].result = 'failed';
          s3Logs[ind1].attachments[ind2].failedReason = result.failedReason;
        }
      });
      if (s3Logs[ind1].attachments[ind2].result === 'failed') {
        failedCnt += 1;
      } else if (s3Logs[ind1].attachments[ind2].result === 'passed') {
        succesCnt += 1;
      } else if (s3Logs[ind1].attachments[ind2].result === 'warning') {
        warningCnt += 1;
      }
    });
  });
  batchLog.attachmentSuccess = succesCnt;
  batchLog.attachmentFailed = failedCnt;
  batchLog.attachemntWarning = warningCnt;
  batchLog.transactions = s3Logs;
  return batchLog;
}

const addAttachment = (docs, type) => new Promise((resolve) => {
  resolve(addAttachmentFunc(docs, type));
});

const replaceDot = (obj) => {
  _.forOwn(obj, (value, key) => {
    // if key has a period, replace all occurences with an underscore
    if (_.includes(key, '.')) {
      const cleanKey = _.replace(key, /\./g, '<|>');
      // eslint-disable-next-line no-param-reassign
      obj[`${cleanKey}`] = value;
      // eslint-disable-next-line no-param-reassign
      delete obj[`${key}`];
    }

    // continue recursively looping through if we have an object or array
    if (_.isObject(value)) {
      return replaceDot(value);
    }
    return null;
  });
  return obj;
};


const getModelByType = (type) => {
  const { importToTestTable, isDebugMode } = systemConfig.debugSetting;
  if (importToTestTable && isDebugMode) {
    return testDataModel;
  }
  switch (type) {
    case 'cust': return customerModel;
    case 'agent': return agentModel;
    case 'userExt': return agentExtraInfoModel;
    case 'bundle': return fnaModel;
    case 'fe': return fnaFeModel;
    case 'na': return fnaNaModel;
    case 'pda': return fnaPdaModel;
    case 'quotation': return quotationModel;
    case 'application': return applicationModel;
    case 'approval': return approvalModel;
    case 'masterApplication': return shieldApplicationModel;
    case 'masterApproval': return shieldApprovalModel;
    case 'seq': return seqMasterModel;
    case 'API_RESPONSE': return apiResponseModel;
    default: return masterDataModel;
  }
};
export {
  insertRecord, getDocByCondition, getDocs, removeDocs, findOneAndUpdate,
  insertMany, getNextID, searchCnt,
  saveRecord, addAttachment, replaceDot, getModelByType,
};
