/* eslint-disable import/no-unresolved */
/* eslint-disable global-require */
/* eslint-disable class-methods-use-this */
import AWS from 'aws-sdk';
import _ from 'lodash';
import s3Config from '../../config/config.json';
import printLogWithTime from '../log';

const fileUtils = require('../fileUtils');

const awsConf = s3Config.awsS3;
// Create an S3 client
// const s3 = new AWS.S3();
// Add KMS, need region
// AWS.config.loadFromPath('app/files/aws/credentials');

const s3Object = new AWS.S3({
  signatureVersion: awsConf.signatureVersion,
  region: awsConf.region,
  accessKeyId: awsConf.accessKeyId,
  secretAccessKey: awsConf.secretAccessKey,
});
// AWS.config.update({
//   accessKeyId: awsConf.accessKeyId,
//   secretAccessKey: awsConf.secretAccessKey,
// });

// printLogWithTime('awsConf.accessKeyId = ',awsConf.accessKeyId);
// printLogWithTime('awsConf.secretAccessKey = ',awsConf.secretAccessKey);
// printLogWithTime('awsConf.KmsID = ',awsConf.KmsID);
// Create a bucket and upload something into it


class s3 extends fileUtils {
  async init() {
    await this.setProxyEnv();
    // printLogWithTime('Set proxy finished');
    return true;
  }

  // gothrough proxy
  setProxyEnv() {
    const isProxy = _.get(s3Config, 'awsS3.isProxy');
    const proxyLink = _.get(s3Config, 'awsS3.proxyLink');
    // if (isProxy) {
    //   printLogWithTime('Proxy enabled = true');
    //   printLogWithTime(`Proxy URL: ${proxyLink}`);
    // } else {
    //   printLogWithTime('Proxy enabled = false');
    // }

    if (isProxy) {
      // printLogWithTime('Proxy starting - 1');
      const HttpProxyAgent = require('https-proxy-agent');
      // printLogWithTime('Proxy starting - 2');
      const proxyAgent = new HttpProxyAgent(proxyLink);
      // printLogWithTime('Proxy starting - 3');
      AWS.config.httpOptions = { agent: proxyAgent };
      // printLogWithTime('Proxy starting - 4');
      // console.log(AWS.config.httpOptions);
    }
  }

  getCredentials() {
    return new Promise((resolve, reject) => {
      AWS.config.getCredentials((err) => {
        if (err) {
          printLogWithTime('credentials error');
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }

  uploadBase64(docId, attachmentType, data, fileType, type) {
    return new Promise((resolve, reject) => {
      const buf = Buffer.from(data.replace(/^data:image\/\w+;base64,/, ''), 'base64');
      const fileKey = this.getFileKeyById(docId, attachmentType);
      const bucketName = `${this.getBucketNameById(docId)}/${type}`;
      const params = {
        Bucket: bucketName,
        Key: fileKey,
        Body: buf,
        ServerSideEncryption: 'aws:kms',
        SSEKMSKeyId: awsConf.KmsID,
        ContentType: fileType, // "image/jpeg"
      };
      const startTime = new Date();
      s3Object.upload(params, (err, dat) => {
        if (err) {
          // console.error(err);
          printLogWithTime(`Error: upload data to ${bucketName}/${fileKey}`);
          reject(err);
        } else {
          if (s3Config.debugSetting.isDebugMode && s3Config.debugSetting.showMorelog) {
            const elapsedTimeMs = new Date() - startTime;
            printLogWithTime(`Successfully uploaded data to ${bucketName}/${fileKey}: Elapsed ${elapsedTimeMs}ms`);
          }
          resolve(dat);
        }
      });
    });
  }


  // uploadBase64(docId, attachmentType, data, fileType, type) {
  uploadBase64Batch(uploadDocs) {
    return new Promise((resolve) => {
      const totolCnt = uploadDocs.length;
      if (totolCnt === 0) {
        resolve();
      }
      let ranCnt = 0;
      const failedCases = [];
      _.forEach(uploadDocs, (doc) => {
        const buf = Buffer.from(doc.data.replace(/^data:image\/\w+;base64,/, ''), 'base64');
        const fileKey = this.getFileKeyById(doc.docId, doc.attachmentId);
        let bucketName = `${this.getBucketNameById(doc.docId)}/${doc.type}`;
        if (doc.masterData || doc.type === 'masterData') {
          bucketName = awsConf.masterBucket;
        }
        const params = {
          Bucket: bucketName,
          Key: fileKey,
          Body: buf,
          ServerSideEncryption: 'aws:kms',
          SSEKMSKeyId: awsConf.KmsID,
          ContentType: doc.fileType, // "image/jpeg"
        };
        const startTime = new Date();
        s3Object.upload(params, (err) => {
          if (err) {
            failedCases.push({
              key: `${doc.docId}-${doc.attachmentId}`,
              name: doc.attachmentId,
              failedReason: err.message,
              result: 'failed',
            });
            ranCnt += 1;
            const elapsedTimeMs = new Date() - startTime;
            printLogWithTime(`Upload failed: ${bucketName}/${fileKey}: Elapsed ${elapsedTimeMs}ms`);
            printLogWithTime(err.message);
          } else {
            /*
            failedCases.push({
              key: `${doc.docId}-${doc.attachmentId}`,
              name: doc.attachmentId,
              failedReason: '',
              result: 'passed',
            }); */
            ranCnt += 1;

            if (s3Config.debugSetting.isDebugMode && s3Config.debugSetting.showMorelog) {
              const elapsedTimeMs = new Date() - startTime;
              printLogWithTime(`Successfully uploaded data to ${bucketName}/${fileKey}: Elapsed ${elapsedTimeMs}ms`);
            }
          }
          if (totolCnt === ranCnt) {
            resolve(failedCases);
          }
        });
      });
    });
  }

  async getAttachment(docId, attachmentType, cb) {
    return new Promise((resolve, reject) => {
      const bucketName = this.getBucketNameById(docId);
      const fileKey = this.getFileKeyById(docId, attachmentType);
      const executeParams = {
        Bucket: bucketName,
        Key: fileKey,
      };
      // const s3 = new AWS.S3({
      //   signatureVersion:'v4',
      //   region:'ap-southeast-1',
      //   accessKeyId: awsConf.accessKeyId,
      //   secretAccessKey: awsConf.secretAccessKey,
      //   });
      // printLogWithTime('getAttachment executeParams= ',executeParams);
      s3Object.getObject(executeParams, (err, data) => {
        if (err) {
          if (reject) {
            reject();
          }
        } else if (resolve) {
          if (typeof cb === 'function') {
            cb(data.Body);
          }
        }
      });
    });
  }

  async getAttachmentUrl(docId, attachmentType, cb) {
    return new Promise(() => {
      const bucketName = this.getBucketNameById(docId);
      const signedUrlExpireSeconds = 60 * 5;
      const fileKey = this.getFileKeyById(docId, attachmentType);
      // fileKey = fileKey + ".jpg";
      const executeParams = {
        Bucket: bucketName,
        Key: fileKey,
        Expires: signedUrlExpireSeconds,
      };
      // const s3 = new AWS.S3({
      //   signatureVersion:awsConf.signatureVersion,
      //   region:awsConf.region,
      //   accessKeyId: awsConf.accessKeyId,
      //   secretAccessKey: awsConf.secretAccessKey,
      //   });
      // printLogWithTime('getAttachmentUrl executeParams= ',executeParams);
      const url = s3Object.getSignedUrl('getObject', executeParams);
      if (url) {
        cb(url);
      }
    });
  }

  async listBuckets() {
    return new Promise((resolve, reject) => {
      s3Object.listBuckets((err) => {
        if (err) {
          reject(err);
        } else {
          resolve(true);
        }
      });
    });
  }
}


module.exports = s3;
